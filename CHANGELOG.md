# Versions 1.2.x

## v1.2.1

Minor feature release

- Support Samba on Ubuntu Bionic; requires an out-of-distro version of adcli (as
version 0.8.2-1ubuntu1.2 that ships with Bionic lacks the `-add-samba-data` flag)

## v1.2.0

Minor bugfix release / major `pdk` nonsense

- Quash warning regarding stdlib's `is_string()` function going away
(but without buying into the kool-aid)
- `pdk convert` cuz apparently, there's no avoiding drinking
that one

# Versions 1.1.x

## v1.1.8

Minor bugfix release

- RH / CentOS 7.9 requires the same fix regarding `pam_krb5.so` as we
did in v1.0.2 for RH8 (i.e. abstain from `authconfig --enablekrb5`)

## v1.1.7

Minor bugfix release

- Rehaul packages_sources.pp and the flow for installing login shells
(in particular `bsh`) on RedHat-ish distributions
- Forbid version 4.4.0 of puppetlabs/inifile (as it requires
a more modern Ruby than RedHat 7.9 is willing to provide)

## v1.1.6

Minor feature release

- Support hosts within the .xaas.epfl.ch domain

## v1.1.5

Minor bugfix release

- Don't lowercase unit group names for $directory_source => "scoldap"

## v1.1.1

Minor bugfix release

- Fix setting kdc = intranet.epfl.ch:88 in /etc/krb5.conf

## v1.1.0

Major bugfix release

- Go back to not requiring $::os to be a structured fact
- Use `forward_pass` instead of `use_first_pass` (which was
  cargo-culted) for the configuration of `pam_sss.so`, thus fixing
  cohabitation with pam_localuser.so on RedHat 7

# Versions 1.0.x

## v1.0.2

Major bugfix release.

As part of some Office 365-related project currently in progress at
EPFL (end of August 2020), we are busy making sense that said field in
AD no longer makes sense from a Kerberos standpoint. This release of
epfl_sso works around the first issue that crops up in this context.

- Fix password-based authentication on RH8
  - No more `pam_krb5`
  - Resist sssd's newly-acquired proclivity to trust `userPrincipalName`; direct it to a nonexistent field instead, as per https://access.redhat.com/solutions/774663

## v1.0.1

Minor bugfix release

- Fix Samba + adjoin and Samba + `use_test_realm => true`

## v1.0.0

- Puppet 6 and RedHat / CentOS 8 support
- Rewrite adjoin script in terms of adcli instead of msktutil


# Versions 0.8.x

## v0.8.5

Minor bugfix release
- Work around yet another locale-related authconfig bug, this time when `sssd.conf` is not pure ASCII (according to the Internet, we seem to be the only ones running into this particular bug)

## v0.8.4

Minor bugfix release
- Ensure sssd gets some traffic prior to abusing its `/var/lib/sss/db/ccache_INTRANET.EPFL.CH` as part of `adjoin status`

## v0.8.3
- Bugfix release
- Ubuntu now requires SASL_NOCANON too
- Deleting krb5.conf on Ubuntu 16.04 prior to running Augeas, had stopped working a while ago; repair
- Fix `msktutil` and `net ads testjoin` checks in `adjoin update status`, which caused false negatives

## v0.8.2

- Bugfix: catalog compile problem on Red Hat / CentOS
- `adjoin` bugfixes:
  - Don't attempt to `net ads testjoin` if Samba is not configured
  - Don't eat `ldapsearch` error messages in case of failure

## v0.8.1

- Bugfix: also configure LDAP and sssd for the test Active Directory
- Use a redundant LDAP connection to the test AD

## v0.8.0

- Give up requiring ad-hoc lines in `/etc/hosts` for the AD servers.
  We are bold enough to believe we tracked down most, perhaps even all
  ways to tell the different Kerberos clients *not* to reverse-resolve
- Support the test Active Directory / Kerberos domain, EXTEST.EPFL.CH
  (pass `$use_test_realm => true` to enable it)

# Version 0.7.x

- Since the [EPFL CA](certauth.epfl.ch) uses a SHA-1 signed root
  certificate, and [Ubuntu Bionic's gnutls has decided to stop
  supporting
  that](http://changelogs.ubuntu.com/changelogs/pool/main/g/gnutls28/gnutls28_3.5.18-1ubuntu1.2/changelog),
  propose to connect to AD's LDAP using Kerberized crypto rather than
  TLS until the AD's certs get sorted out. Operators can opt into the
  old behavior (i.e. trust the vulnerable SHA-1 root CA certificate,
  and keep using TLS) by passing `epflca_is_trusted => true` as a
  parameter to the `epfl_sso` class

# Version 0.6.x

- Explicitly configure /etc/samba/smb.conf for domain membership

- New "adjoin" script to drive all the msktutil business
   - adjoin join OU=foo,...   # Does the interactive part; kinit required prior
   - adjoin update
   - adjoin status            # Goes into /etc/cron.daily/renew-AD-credentials
