# Enable automounting NFS home directories on Linux
#
# === Bibliography:
#
# [Durrer] "Configuration AD NFSv4 Client Ubuntu 16.04-16.10 sur AD de
# Prod et Automap Home mode LDAP", L. Durrer,
# https://sico.epfl.ch:8443/pages/viewpage.action?spaceKey=SIAC&title=IDEVING

class epfl_sso::private::ad_automount_home(
  $autofs_deps                = $::epfl_sso::private::params::autofs_deps,
  $autofs_service             = $::epfl_sso::private::params::autofs_service,
  $ssd_automounts_available_timeout_seconds =
                                $::epfl_sso::private::params::ssd_automounts_available_timeout_seconds
) inherits epfl_sso::private::params {
  ensure_packages($autofs_deps)
  Package[$autofs_deps] ->
  service { $autofs_service:
    ensure    => "running",
    enable    => true,
    subscribe => [Service["sssd"]],
    require => [Anchor["epfl_sso::ad_joined"]]
  }

  # [Durrer] p. 47
  # The rest of the automounter configuration in [Durrer] has been
  # made redundant ever since sssd started brokering the automounter
  # tables (circa 2012)
  name_service { 'automount':
    lookup => ['sss']
  } ~> Service[$autofs_service]

  # Work around https://bugs.launchpad.net/ubuntu/+source/sssd/+bug/1566508
  # on systemd-capable systems; wait for sssd to be able to answer "automount -m"
  # before running autofs. Do this at boot time:
  $_wait_automounts_visible_snippet = "if /usr/sbin/automount -m 2>&1 |grep -q 'map: auto.home'; then exit 0; else sleep 1; fi"
  Package[$autofs_deps] ->
  file { "/etc/systemd/system/${autofs_service}.service.d":
    ensure => "directory"
  } ->
  file { "/etc/systemd/system/${autofs_service}.service.d/wait-for-sssd.conf":
    content => inline_template('# Managed by Puppet, DO NOT EDIT
[Service]
ExecStartPre=/bin/bash -c "for time in $(seq 1 <%= @ssd_automounts_available_timeout_seconds %>); do <%= @_wait_automounts_visible_snippet %>; done; exit 1"
')
  } ~>
  exec { "systemctl daemon-reload; systemctl stop ${autofs_service} || true":
    path => $::path,
    refreshonly => true
  } ~>
  Service[$autofs_service]
}
