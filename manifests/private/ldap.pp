# Manage client-side LDAP configuration and certificates
#
# === Parameters:
#
# $manage_ldap_conf::  Whether to set the Active Directory servers as
#                      the default LDAP servers in ldap.conf
#
# $ldap_conf_path::  Path to ldap.conf file
#
# $ad_servers::      The Active Directory servers to use
#
# $ad_server_base_dn::  Base DN to set in ldap.conf
#
# $ad_krb5_security_factor:: The "security factor" (encryption key
#                            length equivalent) to require from Active
#                            Directory upon Kerberized connection
#                            initialization. Use the largest value
#                            that EPFL's Active Directory will
#                            accomodate at any given time. (Ignored
#                            when $ad_use_tls is true, because apparently
#                            Active Directory cannot walk and chew gum
#                            at the same time; see comment #5 at
#                            https://bugs.launchpad.net/ubuntu/+source/cyrus-sasl2/+bug/1015819)
#
# $ad_cert_is_trusted:: Whether the Active Directory's SSL certificates can
#                       be successfully verified (currently false, because
#                       the EPFL CA that signs it uses SHA-1)
#
# $ad_use_tls::         Whether to use TLS to secure communication with
#                       Active Directory (if false, use Kerberos with
#                       encryption instead)
#
# === Actions:
#
# * Configure ldap.conf (which sssd also uses as a configuration file)

class epfl_sso::private::ldap(
  $ldap_conf_path,
  $ad_servers,
  $ad_server_base_dn,
  $ad_krb5_security_factor,
  $ad_cert_is_trusted,
  $ad_use_tls = $ad_cert_is_trusted,
) inherits epfl_sso::private::params {
  file_line { "BASE in ${ldap_conf_path}":
    path => $ldap_conf_path,
    line => "BASE ${ad_server_base_dn}",
    match => "^#?BASE",
    ensure => "present"
  }

  file_line { "URI in ${ldap_conf_path}":
    path => $ldap_conf_path,
    line => inline_template('URI <%= @ad_servers.map { |h| @ad_use_tls ? "ldaps://#{h}:3269" : "ldap://#{h}" }.join(" ") %>'),
    match => "^#?URI",
    ensure => "present"
  }

  $_use_insecure_ldap_tls = ($ad_use_tls and ! $ad_cert_is_trusted)
  file_line { "TLS_REQCERT in ${ldap_conf_path}":
    path => $ldap_conf_path,
    line => 'TLS_REQCERT never',
    match => "^#? ?TLS_REQCERT",
    ensure => $_use_insecure_ldap_tls ? {
      true => "present",
      default => "absent"
    }
  }
  
  file_line { "SASL_SECPROPS in ${ldap_conf_path}":
    path => $ldap_conf_path,
    line => $ad_use_tls ? {
      true => "SASL_SECPROPS minssf=0,maxssf=0",  # AD cannot use both TLS and GSSAPI,
      # see comment #5 of https://bugs.launchpad.net/ubuntu/+source/cyrus-sasl2/+bug/1015819
      false => "SASL_SECPROPS minssf=${ad_krb5_security_factor}",
    },
    match => "^#? ?(?i:sasl_secprops)",
    ensure => "present"
  }

  # https://bugzilla.redhat.com/show_bug.cgi?id=949864
  file_line { "SASL_NOCANON in ${ldap_conf_path}":
    path => $ldap_conf_path,
    line => "SASL_NOCANON on",
    match => "^#? ?SASL_NOCANON",
    ensure => "present"
  }
}
