# Class: epfl_sso::private::ad
#
# Integrate this computer into EPFL's Active Directory
#
# This class is the translation into Puppet of
# https://fuhm.net/linux-and-active-directory/
#
# Unlike Windows, this approach does *not* preclude cloning - A number
# of VMs can share the same Kerberos credentials with no issues,
# provided $renew_domain_credentials is set to false (or
# alternatively, all clones should have a different hostname)
#
# === Parameters:
#
# $join_domain:: Whether to maintain presence in the Active Directory domain.
#                Puppet will *not* attempt to join the domain proper, as that
#                requires administrative credentials and cannot be done in
#                batch. Instead, an error message will be shown telling you
#                what to do (basically, kinit + use an epfl_sso-provided shell
#                script that drives msktutil(1) et al).
#
# $renew_domain_credentials:: Whether to periodically renew the
#                Kerberos keytab entry. Has no effect under "puppet
#                agent"; RECOMMENDED for "puppet apply" unless this
#                machine is a clonable master that shares the same
#                host name with a number of clones.
#
# $manage_samba_secrets::  Whether to pass --set-samba-secret to msktutil.
#                          Has no effect absent a working, properly configured
#                          Samba installation.
#
# $krb5_realm::  The Active Directory realm to use
#
# $use_test_realm:: If true, also configure the test Active Directory realm.
#                   Default is false
#
# === Actions:
#
# * Deploy pam_krb5.so in an "opportunistic" configuration: grab a TGT
#   if we can, but fail gracefully otherwise
#
# * Optionally (depending on $join_domain), update and validate Active
#   Directory-compatible credentials in /etc/krb5.keytab . Note that
#   cloning virtual machines that are registered in the domain suffers
#   from the same kind of issues as on the Windows platform; as each
#   VM instance will try to update the Kerberos password for the AD
#   entry, they will quickly diverge since only one of them will
#   succeed to do so.
#
# * If running "puppet apply", and if both $join_domain and
#   $renew_domain_credentials are true, set up a crontab to renew the
#   keytab daily

class epfl_sso::private::ad(
  $join_domain,
  $renew_domain_credentials = true,
  $krb5_realm             = $::epfl_sso::private::params::krb5_realm,
  $krb5_test_realm        = $::epfl_sso::private::params::krb5_test_realm,
  $ad_dns_domain          = $::epfl_sso::private::params::ad_dns_domain,
  $ad_server_rrdns        = $::epfl_sso::private::params::ad_server_rrdns,
  $manage_samba_secrets   = $::epfl_sso::private::params::manage_samba_secrets,
  $check_net_ads_testjoin = $::epfl_sso::private::params::check_net_ads_testjoin,
  $use_test_realm         = false
) inherits epfl_sso::private::params {
  Package <| |> ->
  anchor { "epfl_sso::private::ad::adjoin_prereqs": }

  class { "epfl_sso::private::gssapi":
    use_test_realm => $use_test_realm,
  } ->
  # For krb5.conf, kinit etc.
  Anchor["epfl_sso::private::ad::adjoin_prereqs"]

  case $::kernel {
    'Darwin': {
      notice("Mac OS X detected - Skip setup of Active Directory users, groups and authentication")
    }
    'Linux': {
      case $osfamily {
        "Debian": {
          ensure_packages([ "krb5-user"])
        }
        "RedHat": {
          # https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Managing_Smart_Cards/installing-kerberos.html
          ensure_packages(["krb5-workstation", "krb5-libs"])
        }
        default: {
          fail("Not sure how to install Kerberos client-side support on ${osfamily}-family Linux")
        }
      }

      if ($join_domain) {
        if ($osname == "Ubuntu" and versioncmp($osfullrelease, "20") < 0) {
          # Ubuntu Bionic's adcli lacks the --add-samba-data flag
          apt::source { "groovy-epflsso":
            comment  => "Installed by Puppet (epfl-sso) just for the `adcli` package. Please remove when upgrading to Groovy for real.",
            release  => "groovy",
            location => "http://archive.ubuntu.com/ubuntu",
            key      => "630239CC130E1A7FD81A27B140976EAF437D05B5",
            repos    => "main universe multiverse restricted",
            pin      => -10
          } -> Package["adcli"]
          ensure_packages({"adcli" => { install_options => ['-t', 'groovy'] }})
        } else {
          ensure_packages("adcli")
        }

        if ($manage_samba_secrets) {
          # adcli invokes the `net` command behind the scenes (see
          # documentation for its --samba-data-tool flag)
          case $osfamily {
            "Debian": {
              ensure_packages(["samba-common-bin"])
            }
            "RedHat": {
              ensure_packages(["samba-common-tools"])
            }
            default: {
              fail("Not sure how to install the \"net\" command on ${osfamily}-family Linux")
            }
          }
        }

        $_adjoin_path = "/usr/local/sbin/adjoin"

        # The following variables are used in the epfl_sso/adjoin.erb template:
        if ($use_test_realm) {
          $adjoin_krb5_realm = $krb5_test_realm
          $adjoin_dns_domain = $ad_test_dns_domain
        } else {
          $adjoin_krb5_realm = $krb5_realm
          $adjoin_dns_domain = $ad_dns_domain
        }
        file { $_adjoin_path:
          content => template("epfl_sso/adjoin.erb"),
          mode    => "0755"
        } ->
        exec { "${_adjoin_path} update status":
          path => $::path,
          unless => "${_adjoin_path} update status",
          require => [
            Anchor["epfl_sso::samba_configured"],
            Anchor["epfl_sso::private::ad::adjoin_prereqs"],
          ]
        } ->
        anchor { "epfl_sso::ad_joined": }

        if ($renew_domain_credentials and
            $epfl_sso::private::params::is_puppet_apply) {
              package { "moreutils":
                ensure => "installed"  # For the chronic command
              } ->
              file { "/etc/cron.daily/renew-AD-credentials":
                mode => "0755",
                content => inline_template("#!/bin/sh
# Renew keytab, lest Active Directory forget about us after 90 days
#
# Managed by Puppet, DO NOT EDIT

chronic <%= @_adjoin_path %> update status
")
              }
        }
      }   # if ($join_domain)
    }  # case $::kernel is 'Linux'
    default: {
      fail("Unsupported operating system: ${::kernel}")
    }
  }  # case $::kernel
}    # class epfl_sso::private::ad
