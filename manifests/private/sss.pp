# Class: epfl_sso::private::sss
#
# Configure sssd and its clients for integration into SCOLDAP or Active Directory

class epfl_sso::private::sss(
  $ad_server_base_dn,
  $use_test_realm,
  $auth_source,
  $directory_source,
  $ad_automount_home,
  $debug_sssd,
  $manage_nsswitch_netgroup,
  $sssd_packages          = $::epfl_sso::private::params::sssd_packages,
  $sssd_cleanup_globs     = $::epfl_sso::private::params::sssd_cleanup_globs,
  $ad_server_rrdns        = $::epfl_sso::private::params::ad_server_rrdns,
  $ad_servers             = $::epfl_sso::private::params::ad_servers,
  $ad_test_servers        = $::epfl_sso::private::params::ad_test_servers,
  $krb5_realm             = $::epfl_sso::private::params::krb5_realm,
  $krb5_test_realm        = $::epfl_sso::private::params::krb5_test_realm
) inherits epfl_sso::private::params {
  # The following variables are used in the epfl_sso/sssd.conf.erb template:
  if ($use_test_realm) {
    $sssd_krb5_realm = $krb5_test_realm
    $sssd_ad_server  = $ad_test_servers[0]
    $sssd_ad_servers = $ad_test_servers
    $sssd_base_dn    = $ad_server_test_base_dn
  } else {
    $sssd_krb5_realm = $krb5_realm
    $sssd_ad_server  = $ad_server_rrdns
    $sssd_ad_servers = $ad_servers
    $sssd_base_dn    = $ad_server_base_dn
  }

  package { $sssd_packages :
    ensure => present
  } ->
  file { '/etc/sssd/sssd.conf' :
    ensure  => present,
    content => template('epfl_sso/sssd.conf.erb'),
    # The template above uses variables $debug_sssd, $auth_source,
    # $ad_server_rrdns, $ad_server_base_dn, $krb5_realm, $ad_servers
    # and $ad_automount_home
    owner   => root,
    group   => root,
    mode    => '0600'
  } ~>
  service { 'sssd':
    ensure  => running,
    enable  => true,
    restart => ["/bin/bash", "-c", "set -e -x; service sssd stop; rm ${sssd_cleanup_globs}; service sssd start"]
  }

  include epfl_sso::private::pam
  epfl_sso::private::pam::module { "sss": }

  name_service {['passwd', 'group']:
    lookup => ['compat', 'sss']
  }

  # This is necessary for RH7 and CentOS 7, and probably
  # does not hurt for older versions:
  name_service { 'initgroups':
    # https://bugzilla.redhat.com/show_bug.cgi?id=751450
    lookup => ['files [SUCCESS=continue] sss']
  }

  if ($manage_nsswitch_netgroup) {
    name_service { 'netgroup':
      lookup => ['files', 'sss']
    }
  }
}
