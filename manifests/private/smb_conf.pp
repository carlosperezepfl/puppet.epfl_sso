class epfl_sso::private::smb_conf(
  $use_test_realm,
  $smb_conf_file = $epfl_sso::private::params::smb_conf_file
) inherits epfl_sso::private::params {

  $_smb_workgroup = $use_test_realm ? {
    true    => "EXTEST",
    default => "INTRANET"
  }

  $_smb_realm = $use_test_realm ? {
    true    => "EXTEST.EPFL.CH",
    default => "INTRANET.EPFL.CH"
  }

  ini_setting { "[global] security":
    path    => $smb_conf_file,
    section => 'global',
    setting => 'security',
    value   => 'ads'
  }

  ini_setting { "[global] workgroup":
    path    => $smb_conf_file,
    section => 'global',
    setting => 'workgroup',
    value   => $_smb_workgroup
  }

  ini_setting { "[global] realm":
    path    => $smb_conf_file,
    section => 'global',
    setting => 'realm',
    value   => $_smb_realm
  }
}
